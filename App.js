import * as React from 'react';
import {FlatList, StyleSheet, Text, View, Button, TextInput, Alert, ScrollView, Dimensions} from 'react-native'
import {Constants} from 'expo';
import { createStackNavigator, createAppContainer } from 'react-navigation';

//var deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({

  container: {
    paddingTop : Constants.statusBarHeight,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
    flexWrap: 'wrap'
  },

  notes: {
    padding: 5,
    //height: deviceHeight-200,
    backgroundColor: 'lightgrey'
  },

  button : {
        margin: 16,
        bottom: 10,
        color: 'lightskyblue' 
  }
})

export default class List extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      Holder: '',
      list: [
          {key: 'Note 1'},
          {key: 'Note 2'},
          {key: 'Note 3'},
          {key: 'Note 4'},
          {key: 'Note 5'},
          {key: 'Note 6'},
          {key: 'Note 7'},
          {key: 'Note 8'},
          {key: 'Note 9'},
          {key: 'Note 10'}
        ]
    }
  }
AddItemsToArray =()=> {
  var dupli = false
  for (var i=0;i<this.state.list.length;i++) {
    if(this.state.list[i].key == this.state.Holder.toString()) {
      Alert.alert("I can't let you do that.")
      dupli = true
    }
  }
  if (dupli==false) {
    this.setState ({ list: this.state.list.concat({'key':this.state.Holder.toString()})});} 
  }

  render() {
    return (
      <ScrollView>
      <View style={styles.container}>
        <View style={styles.notes}>
          <FlatList
            data = {this.state.list}
            renderItem ={({item}) => <Text>{item.key}</Text>}
          />
        </View>
        <View>
          <TextInput
          style={{height: 40, fontSize: 20}}
          placeholder="Write the note here"
          onChangeText={TextInputValue => this.setState({ Holder : TextInputValue }) }
        />
        </View>
        <View style={styles.button}>
          <Button style={styles.button}
            title="ADD NOTE"
            onPress={this.AddItemsToArray}
          />  
        </View>
      </View>
      </ScrollView>
    );
  }
}